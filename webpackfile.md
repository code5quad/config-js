# Crie um arquivo nomeado - webpack.config.js

* Coloque o código abaixo no arquivo

```js
module.exports = {
    entry: ['@babel/polyfill', './src/main.js'],
    output:{
      path: __dirname + '/public',
      filename: 'bundle.js',  
    },
    devServer:{
        contentBase: __dirname + '/public',
    },
    module:{
        rules: [
          {
              test: /\.js$/,
              exclude: /node_modules/,
              use:
              {
                  loader: 'babel-loader',
              }
          }
        ],
    },
};

```

* entry - indica nosso arquivo principal;
* output - indica o arquivo de saida (bundle.js);
* module-rules - pega todos os arquivos js da aplicação.

