# Configurando WEBPACK

- Vá no terminal e digite os códigos abaixo (Com yarn)

- yarn add webpack webpack-cli -D

- yarn add babel-loader -D

- yarn add webpack-dev-server -D

- yarn add @babel/plugin-transform-async-to-generator -D

- yarn add @babel/polyfill -D

- vá no package.json e coloque o seguinte código (scripts),
depois do obejto devDependencies:

```json
  "scripts": {
    "dev": "webpack-dev-server --mode=development",
    "build": "webpack --mode=production"
  }

```

-----------------------------------------------------------------

- Vá no terminal e digite os códigos abaixo (Com npm)

- npm install webpack webpack-cli -D

- npm install babel-loader -D

- npm install webpack-dev-server -D

- npm install @babel/plugin-transform-async-to-generator -D

- npm install @babel/polyfill -D

- vá no package.json e coloque o seguinte código (scripts),
depois do obejto devDependencies:

```json
  "scripts": {
    "dev": "webpack-dev-server --mode=development",
    "build": "webpack --mode=production"
  }
```