# Configurando BABEL

- Abra o terminal e digite os códigos abaixo (Com yarn)

- yarn init;

- yarn add @babel/cli;

- yarn add @babel/preset-env;

- yarn add @babel/core;

- yarn add @babel/plugin-proposal-object-rest-spread;

- dependencies no package.json vira devDependencies;

------------------------------------------------------------------

- Abra o terminal e digite os códigos abaixo (Com npm)

- npm init;

- npm install @babel/cli;

- npm install @babel/preset-env;

- npm install @babel/core;

- npm install @babel/plugin-proposal-object-rest-spread;

- dependencies no package.json vira devDependencies;


