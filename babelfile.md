# Crie um arquivo nomeado - .babelrc

* Coloque o c�digo abaixo no arquivo

```json
{
    "presets": ["@babel/preset-env"],
    "plugins": [
        "@babel/plugin-proposal-object-rest-spread",
        "@babel/plugin-transform-async-to-generator"
    ]
}

```