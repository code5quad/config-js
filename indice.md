# Para fazer tudo isso você precisa ter instalado: node.js, git e o yarn(optional)

### Caso não queira usar o yarn, use o NPM que vem com o node.js



* [Configure o babel](configBabel.md);
* [Configure o webpack](configWebpack.md);
* [Configure o arquivo - .babelrc](babelfile.md);
* [Configure o arquivo - webpack.config.js](webpackfile.md);
* [Crie as pastas](pastas.md);

* Depois de tudo feito, digite no terminal "yarn dev" ou "npm run dev".
 